import docplex.mp.model

def solveStochasticProblem(T, data, demandScenarios, startingInventories, lastPriceLevels, changeCounters, lastChangePeriod, restrictionType):
    mipGap, logOutput = 0.01, False
    P, P_ext, L, r, U, U_ext, C, h, A, s, realizedDemands, maxChangesPerProduct, minDistanceBetweenPriceChanges = data
    mdl = docplex.mp.model.Model()

    gamma = mdl.continuous_var_dict(T)
    phi = mdl.continuous_var_dict([(p, t, l) for p in P for t in T for l in L])
    z = mdl.binary_var_dict([(p, t, l) for p in P for t in T for l in L])
    D = mdl.continuous_var_dict([(p, t, xi) for p in P for t in T for xi in demandScenarios])
    x = mdl.continuous_var_dict([(p, t, xi) for p in P for t in T for xi in demandScenarios])
    y = mdl.binary_var_dict([(p, t, xi) for p in P for t in T for xi in demandScenarios])    
    I = mdl.continuous_var_dict([(p, t, xi) for p in P for t in T for xi in demandScenarios])
    S = mdl.continuous_var_dict([(p, t, xi) for p in P for t in T for xi in demandScenarios])
    shares = mdl.continuous_var_dict([(p, t) for p in P for t in T])
   
    revenues = mdl.sum(r[p, l] * U[p, l] * phi[p, t, l] * demandScenarios[xi][t] for p in P for t in T for l in L for xi in demandScenarios)
    inventoryCosts = mdl.sum(h[p, t] * I[p, t, xi] for p in P for t in T for xi in demandScenarios)
    setupCosts = mdl.sum(A[p, t] * y[p, t, xi] for p in P for t in T for xi in demandScenarios)
    shortageCosts = mdl.sum(s[p, t] * S[p, t, xi] for p in P for t in T for xi in demandScenarios)
    objective = revenues - (inventoryCosts + setupCosts + shortageCosts) / len(demandScenarios)
    mdl.maximize(objective)
    
    mdl.add_constraints(mdl.sum(U_ext[p] * gamma[t] for p in P_ext) + mdl.sum(U[p, l] * phi[p, t, l] for p in P for l in L) == 1 for t in T)
    mdl.add_constraints(phi[p, t, l] <= 9999 * z[p, t, l] for p in P for t in T for l in L)
    mdl.add_constraints(phi[p, t, l] <= gamma[t] for p in P for t in T for l in L)
    mdl.add_constraints(phi[p, t, l] >= gamma[t] + 9999 * (z[p, t, l] - 1) for p in P for t in T for l in L)
    mdl.add_constraints(mdl.sum(z[p, t, l] for l in L) == 1 for p in P for t in T)
    intervals = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16],[17,18,19,20,21],[22,23,24,25],[26,27,28,29,30],[31,32,33,34],[35,36,37,38],[39,40,41,42,43],[44,45,46,47],[48,49,50,51,52]]
    for interval in intervals:
        for i in range(0, len(interval) - 1):
            if min(T) - 1 + interval[i] + 1 <= max(T):
                mdl.add_constraints(z[p, min(T) - 1 + interval[i], l] == z[p, min(T) - 1 + interval[i] + 1, l] for p in P for l in L)
                mdl.add_constraints(phi[p, min(T) - 1 + interval[i], l] == phi[p, min(T) - 1 +  interval[i] + 1, l] for p in P for l in L)
                mdl.add_constraint(gamma[min(T) - 1 + interval[i]] == gamma[min(T) - 1 +  interval[i] + 1])
    
    mdl.add_constraints(D[p, t, xi] == demandScenarios[xi][t] * mdl.sum(U[p, l] * phi[p, t, l] for l in L) for p in P for t in T for xi in demandScenarios) 
    mdl.add_constraints(I[p, t, xi]  == startingInventories[p, t - 1] + x[p, t, xi] + S[p, t, xi] - D[p, t, xi] for p in P for t in T if t == min(T) for xi in demandScenarios)
    mdl.add_constraints(I[p, t, xi]  == I[p, t - 1, xi] + x[p, t, xi] + S[p, t, xi] - D[p, t, xi] for p in P for t in T if t > min(T) for xi in demandScenarios)
    mdl.add_constraints(mdl.sum(x[p, t, xi] for p in P) <= C[t] for t in T for xi in demandScenarios)
    mdl.add_constraints(x[p, t, xi] <= 9999 * y[p, t, xi] for p in P for t in T for xi in demandScenarios)
    
    mdl.add_constraints(shares[p, t] == 1 - mdl.sum(U_ext[p2] * gamma[t] for p2 in P_ext) - mdl.sum(U[p2, l] * phi[p2, t, l] for p2 in P if p2 != p for l in L) for p in P for t in T)
    
    if restrictionType == "number" or restrictionType == "distance":
        Delta = mdl.continuous_var_dict([(p, t, l) for p in P for t in T for l in L])
        if min(T) > 1:
            mdl.add_constraints(1 - z[p, t, l] + (lastPriceLevels[p, t - 1] == l) + Delta[p, t, l] >= 1 for p in P for t in T if t == min(T) for l in L)
            mdl.add_constraints(z[p, t, l] + 1 - (lastPriceLevels[p, t - 1] == l) + Delta[p, t, l] >= 1 for p in P for t in T if t == min(T) for l in L)
            mdl.add_constraints(3 - z[p, t, l] - (lastPriceLevels[p, t - 1] == l) - Delta[p, t, l] >= 1 for p in P for t in T if t == min(T) for l in L)
            mdl.add_constraints(z[p, t, l] + (lastPriceLevels[p, t - 1] == l) + 1 - Delta[p, t, l] >= 1 for p in P for t in T if t == min(T) for l in L)
        mdl.add_constraints(1 - z[p, t, l] + z[p, t - 1, l] + Delta[p, t, l] >= 1 for p in P for t in T if t > min(T) for l in L)
        mdl.add_constraints(z[p, t, l] + 1 - z[p, t - 1, l] + Delta[p, t, l] >= 1 for p in P for t in T if t > min(T) for l in L)
        mdl.add_constraints(3 - z[p, t, l] - z[p, t - 1, l] - Delta[p, t, l] >= 1 for p in P for t in T if t > min(T) for l in L)
        mdl.add_constraints(z[p, t, l] + z[p, t - 1, l] + 1 - Delta[p, t, l] >= 1 for p in P for t in T if t > min(T) for l in L)
    if restrictionType == "number":
        mdl.add_constraints(mdl.sum(Delta[p, t, l] for t in T for l in L) <= 2 * (maxChangesPerProduct - changeCounters[p]) for p in P)
    if restrictionType == "distance":
        D_intervals = []
        for t in range(min(T), max(T)):
            if t + minDistanceBetweenPriceChanges - 1 <= max(T):
                D_intervals.append(range(t, t + minDistanceBetweenPriceChanges))
        mdl.add_constraints(mdl.sum(Delta[p, t, l] for t in D_interval for l in L) <= 2 for p in P for D_interval in D_intervals)
        for p in P:
            truncated_D_interval = []
            for t in range(lastChangePeriod[p], lastChangePeriod[p] + minDistanceBetweenPriceChanges):
                if t >= min(T) and t <= max(T):
                    truncated_D_interval.append(t)
            mdl.add_constraint(mdl.sum(Delta[p, t, l] for t in truncated_D_interval for l in L) == 0)    
    
    mdl.parameters.mip.tolerances.mipgap = mipGap
    mdl.parameters.timelimit = 120
    mdl.solve(log_output = logOutput)
    return mdl.solution.get_value(objective), mdl.solution.get_value_dict(shares), mdl.solution.get_value_dict(z)

def solveLotSizing(T, data, demands, startingInventories):
    P, P_ext, L, r, U, U_ext, C, h, A, s, realizedDemands, maxChangesPerProduct, minDistanceBetweenPriceChanges = data
    mdl = docplex.mp.model.Model()
    
    x = mdl.continuous_var_dict([(p, t) for p in P for t in T])
    y = mdl.binary_var_dict([(p, t) for p in P for t in T])
    I = mdl.continuous_var_dict([(p, t) for p in P for t in T])
    LS = mdl.continuous_var_dict([(p, t) for p in P for t in T])
    
    inventoryCosts = mdl.sum(h[p, t] * I[p, t] for p in P for t in T)
    setupCosts = mdl.sum(A[p, t] * y[p, t] for p in P for t in T)
    shortageCosts = mdl.sum(s[p, t] * LS[p, t] for p in P for t in T)
    objective = inventoryCosts + setupCosts + shortageCosts
    mdl.minimize(objective)
    
    mdl.add_constraints(I[p, t] == startingInventories[p, t - 1] + x[p, t] + LS[p, t] - demands[p, t] for p in P for t in T if t == min(T))
    mdl.add_constraints(I[p, t] == I[p, t - 1] + x[p, t] + LS[p, t] - demands[p, t] for p in P for t in T if t > min(T))
    mdl.add_constraints(mdl.sum(x[p, t] for p in P) <= C[t] for t in T)
    mdl.add_constraints(x[p, t] <= 9999 * y[p, t] for p in P for t in T)
    
    mdl.solve(log_output = False)
    return mdl.solution.get_value_dict(x)