import tools
import mipModels
import time

def solvePricing(seed, T, data, inventories, priceLevels, changeCounters, lastChangePeriod, t, N, restrictionType):
    P, P_ext, L, r, U, U_ext, C, h, A, s, realizedDemands, maxChangesPerProduct, minDistanceBetweenPriceChanges = data
    objectiveList, sharesList, zList = [], [], []
    M = 1
    for m in range(0, M):
        demandScenarios = tools.createDemandScenarios(realizedDemands, t, N, seed + 12345*m)
        if N == 0:
            demandScenarios = {0: realizedDemands}
        objective, shares, z = mipModels.solveStochasticProblem(T, data, demandScenarios, inventories, priceLevels, changeCounters, lastChangePeriod, restrictionType)
        objectiveList.append(objective)
        sharesList.append(shares)
        zList.append(z)
    best_m = objectiveList.index(max(objectiveList))
    return sharesList[best_m], zList[best_m]
        
def performRH(seed, h_P, h_LS, t_P_values, t_LS_values, num_P, num_P_ext, num_L, N, restrictionType):
    startTime = time.time()
    data = tools.getData(seed, h_P, num_P, num_P_ext, num_L)
    P, P_ext, L, r, U, U_ext, C, h, A, s, realizedDemands, maxChangesPerProduct, minDistanceBetweenPriceChanges = data
    priceLevels, lotSizes, inventories, lostSales, changeCounters, lastChangePeriod = {}, {}, {(p, 0): 0 for p in P}, {}, {p: 0 for p in P}, {p: 0 for p in P}
    revenues, inventoryCosts, setupCosts, shortageCosts = 0, 0, 0, 0
    for t in range(1, h_P + 1):
        if t in t_P_values:
            T_P = range(t, t + h_P)
            shares, z = solvePricing(seed, T_P, data, inventories, priceLevels, changeCounters, lastChangePeriod, t, N, restrictionType)
        if t in t_LS_values:
            T_LS = range(t, t + h_LS)
            anticipatedDemands = tools.createDemandScenarios(realizedDemands, t, 1, seed + 6789*t)[0]
            if N == 0:
                anticipatedDemands = realizedDemands
            anticipatedDemandsOfProductsInPeriods = tools.getDemandsOfProductsInPeriods(anticipatedDemands, shares)            
            x = mipModels.solveLotSizing(T_LS, data, anticipatedDemandsOfProductsInPeriods, inventories)
        for p in P:
            priceLevels[p, t], lotSizes[p, t], inventories[p, t], lostSales[p, t] = round(sum(l * z[p, t, l] for l in L)), x[p, t], max(0, inventories[p, t - 1] + x[p, t] - shares[p, t] * realizedDemands[t]), max(0, -(inventories[p, t - 1] + x[p, t] - shares[p, t] * realizedDemands[t]))
            revenues, inventoryCosts, setupCosts, shortageCosts = revenues + sum(r[p, l] * z[p, t, l] for l in L) * shares[p, t] * realizedDemands[t], inventoryCosts + h[p, t] * inventories[p, t], setupCosts + A[p, t] * (x[p, t] > 0.001), shortageCosts + s[p, t] * lostSales[p, t]
            if t > 1 and priceLevels[p, t] != priceLevels[p, t - 1]:
                changeCounters[p] += 1
                lastChangePeriod[p] = t
        #if t in t_P_values:
            #print("T", T_P)            
            #print("PriceLevels", priceLevels)
            #print("LotSizes", lotSizes)
            #print("LostSales", lostSales)
    priceChanges = 0
    for p in P:
        for t in range(2, 53):
            if abs(priceLevels[p, t] - priceLevels[p, t-1]) > 0.5:
                priceChanges += 1
    endTime = time.time()
    return revenues - inventoryCosts - setupCosts - shortageCosts, revenues, -inventoryCosts, -setupCosts, -shortageCosts, endTime - startTime, priceChanges

h_P, h_LS, t_P_values, t_LS_values = 52, 26, [1, 5, 9, 13, 17, 22, 26, 31, 35, 39, 44, 48], range(1, 53)
#num_P_vals, num_P_ext_vals, num_L_vals, N_vals, restriction_vals, seed_vals = [1, 2, 3, 4], [3], [3, 5, 10], [0, 5, 10, 15, 20, 25], ["none", "number", "distance"], range(0, 20)
num_P_vals, num_P_ext_vals, num_L_vals, N_vals, restriction_vals, seed_vals = [4], [3], [3, 5], [0, 5, 10, 15, 20, 25], ["none", "number", "distance"], range(0, 20)
evaluations = []
for num_P_val in num_P_vals:
    for num_P_ext_val in num_P_ext_vals:
        for num_L_val in num_L_vals:
            for N_val in N_vals:
                for restriction_val in restriction_vals:
                    for seed_val in seed_vals:
                        if (num_L_val == 3	and N_val > 10) or num_L_val == 5 or (num_L_val == 3 and N_val == 10 and restriction_val == "distance" and seed_val > 15):
                            profit, revenues, inventoryCosts, setupCosts, shortageCosts, computeTime, priceChanges = performRH(seed_val, h_P, h_LS, t_P_values, t_LS_values, num_P_val, num_P_ext_val, num_L_val, N_val, restriction_val)
                            evaluation = {"num_P": num_P_val, "num_P_ext": num_P_ext_val, "num_L": num_L_val, "N": N_val, "restriction": restriction_val, "seed": seed_val, "profit": profit, "revenues": revenues, "inventoryCosts": inventoryCosts, "setupCosts": setupCosts, "shortageCosts": shortageCosts, "computeTime": computeTime, "priceChanges": priceChanges}
                            evaluations.append(evaluation)
                            print(evaluation)
                            tools.writeToResultFile(evaluations)