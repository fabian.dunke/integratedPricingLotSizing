import math
import random
import pandas

def getData(seed, h_P, num_P, num_P_ext, num_L):
    rng = random.Random(seed)
    P, P_ext, L = range(1, num_P + 1), range(1, num_P_ext + 1), range(1, num_L + 1)        
    
    r = {}
    for p in P:
        priceLevels = sorted(rng.sample(range(1, 16), len(L)))
        for l in L:
            r[p, l] = priceLevels[l - 1]            
    U = {}
    for p in P:
        priceSensitivity = rng.uniform(-0.75, -0.25)
        for l in L:
            U[p, l] = math.exp(priceSensitivity * r[p, l])
    U_ext = {p: math.exp(rng.uniform(-0.75, -0.25) * rng.randint(1, 16)) for p in P_ext}
    C = {t: num_P * rng.randrange(50, 250) for t in range(1, 2 * h_P + 1)}        
    h = {(p, t): rng.uniform(0.1, 0.5) for p in P for t in range(1, 2 * h_P + 1)} # hier sensibel 0.5 --> 2.5
    A = {(p, t): rng.uniform(25, 75) for p in P for t in range(1, 2 * h_P + 1)} # hier sensibel 75 --> 175
    s = {(p, t): 10 for p in P for t in range(1, 2 * h_P + 1)}    
    realizedDemands = {t: (num_P + num_P_ext) * rng.randrange(50, 250) for t in range(1, 2 * h_P + 1)}
    maxChangesPerProduct = 3
    minDistanceBetweenPriceChanges = 12

    data = P, P_ext, L, r, U, U_ext, C, h, A, s, realizedDemands, maxChangesPerProduct, minDistanceBetweenPriceChanges
    return data

def createDemandScenarios(baseScenario, t_current, N, seed):
    rng2 = random.Random(seed)
    periods = baseScenario.keys()
    demandScenarios = {}
    for n in range(0, N):
        scenario = {t: rng2.uniform(0.75, 1.25) * baseScenario[t] for t in periods}
        num_UpPhases, num_DownPhases = rng2.randint(0,2), rng2.randint(0,2)        
        
        startingPeriods_UpPhases, startingPeriods_DownPhases = sorted(rng2.sample(periods, num_UpPhases)), sorted(rng2.sample(periods, num_DownPhases))
        upPeriods, downPeriods = [], []
        for i in range(0, num_UpPhases):
            upPeriods.append(list(range(startingPeriods_UpPhases[i], min(startingPeriods_UpPhases[i] + rng2.randint(1, 10), max(periods)) + 1)))
        for i in range(0, num_DownPhases):
            downPeriods.append(list(range(startingPeriods_DownPhases[i], min(startingPeriods_DownPhases[i] + rng2.randint(1, 10), max(periods)) + 1)))
        upPeriods, downPeriods = sorted([item for sublist in upPeriods for item in sublist]), sorted([item for sublist in downPeriods for item in sublist])

        for t in periods:
            if t in downPeriods:
                scenario[t] = 0.75 * scenario[t]
            if t in upPeriods:
                scenario[t] = 1.25 * scenario[t]
            if t < t_current:
                scenario[t] = baseScenario[t]
        demandScenarios[n] = scenario        
    return demandScenarios

def getProjectionOfDictionary(dictionary, dimension, value):
    projection = {}
    for key in dictionary.keys():
        if key[dimension] == value:
            projection[key] = dictionary[key]
    return projection

def getDemandsOfProductsInPeriods(demands, shares):
    return {(p, t): shares[p, t] * demands[t] for (p, t) in shares.keys()}

def writeToResultFile(evaluations):
    df = pandas.DataFrame(data=evaluations)
    writer = pandas.ExcelWriter('overallResults.xlsx') 
    df.style.set_properties(**{'text-align': 'center'}).to_excel(writer, sheet_name='Summary', index=False, na_rep='NaN')
    writer.sheets['Summary'].set_column(0, 0, 10)
    for i in range(1, 48):
        writer.sheets['Summary'].set_column(i, i, 12)
    writer.save()